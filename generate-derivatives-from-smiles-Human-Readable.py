import ase
from ase.io import read, write
from ase.calculators.gaussian import Gaussian,GaussianOptimizer
from openbabel import pybel
import openbabel as ob
import itertools
import os

"""openbabel may be difficult to install as it requires some compilation on your PC. The package 
openbabel in pip depends on the presence of libopenbabel.so; this may be installed from your distribution's
package manager. Alternatively, prebuilt binaries are available; see https://open-babel.readthedocs.io/en/latest/Installation/install.html#install-binaries


Finally, there is the cinfony package "https://cinfony.github.io/"; this appears to have many different
cheminformatics packages in one batch, and allows you to install them without dealing with compiling
various bits and pieces. 

Generate plausible 3d structures for each of the 16 desired molecules
Further optimization with gaussview is recommended before submitting for a geometry optimization job
"""
ptable = {'0':'X','1': 'H', '2': 'He', '3': 'Li', '4': 'Be', '5': 'B', '6': 'C', '7': 'N', '8': 'O', '9': 'F', '10': 'Ne', '11': 'Na', '12': 'Mg', '13': 'Al', '14': 'Si', '15': 'P', '16': 'S', '17': 'Cl', '18': 'Ar', '19': 'K', '20': 'Ca', '21': 'Sc', '22': 'Ti', '23': 'V', '24': 'Cr', '25': 'Mn', '26': 'Fe', '27': 'Co', '28': 'Ni', '29': 'Cu', '30': 'Zn', '31': 'Ga', '32': 'Ge', '33': 'As', '34': 'Se', '35': 'Br', '36': 'Kr', '37': 'Rb', '38': 'Sr', '39': 'Y', '40': 'Zr', '41': 'Nb', '42': 'Mo', '43': 'Tc', '44': 'Ru', '45': 'Rh', '46': 'Pd', '47': 'Ag', '48': 'Cd', '49': 'In', '50': 'Sn', '51': 'Sb', '52': 'Te', '53': 'I', '54': 'Xe', '55': 'Cs', '56': 'Ba', '57': 'La', '58': 'Ce', '59': 'Pr', '60': 'Nd', '61': 'Pm', '62': 'Sm', '63': 'Eu', '64': 'Gd', '65': 'Tb', '66': 'Dy', '67': 'Ho', '68': 'Er', '69': 'Tm', '70': 'Yb', '71': 'Lu', '72': 'Hf', '73': 'Ta', '74': 'W', '75': 'Re', '76': 'Os', '77': 'Ir', '78': 'Pt', '79': 'Au', '80': 'Hg', '81': 'Tl', '82': 'Pb', '83': 'Bi', '84': 'Po', '85': 'At', '86': 'Rn', '87': 'Fr', '88': 'Ra', '89': 'Ac', '90': 'Th', '91': 'Pa', '92': 'U', '93': 'Np', '94': 'Pu', '95': 'Am', '96': 'Cm', '97': 'Bk', '98': 'Cf', '99': 'Es', '100': 'Fm', '101': 'Md', '102': 'No', '103': 'Lr', '104': 'Rf', '105': 'Db', '106': 'Sg', '107': 'Bh', '108': 'Hs', '109': 'Mt', '110': 'Ds', '111': 'Rg', '112': 'Cn', '113': 'Nh', '114': 'Fl', '115': 'Mc', '116': 'Lv', '117': 'Ts', '118': 'Og'}

modifiers = {"EDG":["N","[H]"],"EWG":["N([O])[O]","c2ccccc2"]}
modifiers_HR = {"N":"amine","[H]":"hydrogen","N([O])[O]":"nitrate","c2ccccc2":"phenyl"}

"""modifiers: a dictionary of dictionaries containing interesting groups, represented as a SMILES string;
modifiers_HR: a dictionary with keys==a smiles string, and the value being the human-readable name for the string
"""

template = "-STRING-C1=CC=[N]({{R1}})[Ga]([N]({{R2}}))=[N]1{{R1}}"
NUMRGROUPS = 2

"""it is convenient to draw the basic structure using something like molview.org and copy the smiles strings.
use an unexpected atom for the templating positions.
"""
def generateOptions(template,modifiers,NUMRGROUPS):
    strings = []
    strings_HR = []
    templates = ["{{R"+str(i+1)+"}}" for i in range(NUMRGROUPS)]
    mods = []
    for key in modifiers.keys():
        mods.extend(modifiers[key])
    for comb in itertools.product(mods,repeat=NUMRGROUPS):
        print(comb)
        c = template.replace("-STRING-","")
        c_HR = "R1-{{R1}}-R2-{{R2}}"
        for i,entry in enumerate(comb):
            c = c.replace(templates[i],entry)
            c_HR = c_HR.replace(templates[i],modifiers_HR[entry])
        print(c_HR)
        strings.append(c)
        strings_HR.append(c_HR)
    return strings, strings_HR
def getNumElectrons(mol):
    u = 0
    for atom in mol:
        u = u+atom.number
    return u
def make3D(molstring):
    mymol = pybel.readstring("smi",molstring)
    gen3d = ob.OBOp.FindType("gen3D")
    gen3d.Do(mymol.OBMol,"--best")
    structure = ase.Atoms()
    for atom in mymol.atoms:
        structure.append(ptable[str(atom.atomicnum)])
        structure[-1].position = atom.coords
    return structure

def main():
    moles,moles_HR = generateOptions(template,modifiers,NUMRGROUPS)
    for i,mole in enumerate(moles):
        struct = make3D(mole)
        print(getNumElectrons(struct))
        struct.calc = Gaussian(label='calc/gaussian',
                xc='wb97xd',
                basis='6-311++g(d,p)',
                scf='maxcycle=100')
        write(moles_HR[i]+".gjf",struct)
        """Write a gaussian job file (gjf)
        The following lines simply edit the file to create the headers needed to make a 
        geometry optimization job. See gaussian docs for how to create a job for various
        other job types you may wish to run."""
        with open(moles_HR[i]+".gjf","r") as f:
            contents = f.read()
            contents = contents.replace("#P","")
        with open(moles_HR[i]+".gjf","w") as f:
            f.write("""%NProcShared=64
%chk=final.chk
%mem=32GB
# opt wb97xd/6-311++g(d,p)""" + contents)
        os.system('submit_g16.pl 64 24 0 "'+mole+'"')
#main()