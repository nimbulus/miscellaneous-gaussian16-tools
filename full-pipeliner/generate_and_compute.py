import ase
from ase.io import read, write
from ase.calculators.gaussian import Gaussian,GaussianOptimizer
from catkit import Gratoms
from catkit.gen import molecules
from openbabel import pybel
import openbabel as ob
import itertools
import os
from pysmiles import read_smiles as to_networkx
import networkx as nx
import numpy as np
import gratoms_relax

ptable = {'0':'X','1': 'H', '2': 'He', '3': 'Li', '4': 'Be', '5': 'B', '6': 'C', '7': 'N', '8': 'O', '9': 'F', '10': 'Ne', '11': 'Na', '12': 'Mg', '13': 'Al', '14': 'Si', '15': 'P', '16': 'S', '17': 'Cl', '18': 'Ar', '19': 'K', '20': 'Ca', '21': 'Sc', '22': 'Ti', '23': 'V', '24': 'Cr', '25': 'Mn', '26': 'Fe', '27': 'Co', '28': 'Ni', '29': 'Cu', '30': 'Zn', '31': 'Ga', '32': 'Ge', '33': 'As', '34': 'Se', '35': 'Br', '36': 'Kr', '37': 'Rb', '38': 'Sr', '39': 'Y', '40': 'Zr', '41': 'Nb', '42': 'Mo', '43': 'Tc', '44': 'Ru', '45': 'Rh', '46': 'Pd', '47': 'Ag', '48': 'Cd', '49': 'In', '50': 'Sn', '51': 'Sb', '52': 'Te', '53': 'I', '54': 'Xe', '55': 'Cs', '56': 'Ba', '57': 'La', '58': 'Ce', '59': 'Pr', '60': 'Nd', '61': 'Pm', '62': 'Sm', '63': 'Eu', '64': 'Gd', '65': 'Tb', '66': 'Dy', '67': 'Ho', '68': 'Er', '69': 'Tm', '70': 'Yb', '71': 'Lu', '72': 'Hf', '73': 'Ta', '74': 'W', '75': 'Re', '76': 'Os', '77': 'Ir', '78': 'Pt', '79': 'Au', '80': 'Hg', '81': 'Tl', '82': 'Pb', '83': 'Bi', '84': 'Po', '85': 'At', '86': 'Rn', '87': 'Fr', '88': 'Ra', '89': 'Ac', '90': 'Th', '91': 'Pa', '92': 'U', '93': 'Np', '94': 'Pu', '95': 'Am', '96': 'Cm', '97': 'Bk', '98': 'Cf', '99': 'Es', '100': 'Fm', '101': 'Md', '102': 'No', '103': 'Lr', '104': 'Rf', '105': 'Db', '106': 'Sg', '107': 'Bh', '108': 'Hs', '109': 'Mt', '110': 'Ds', '111': 'Rg', '112': 'Cn', '113': 'Nh', '114': 'Fl', '115': 'Mc', '116': 'Lv', '117': 'Ts', '118': 'Og'}

modifiers = {"EDG":["N","[H]"],"EWG":["N([O])[O]","c2ccccc2"]}
modifiers_HR = {"N":"amine","[H]":"hydrogen","N([O])[O]":"nitrate","c2ccccc2":"phenyl"}

"""modifiers: a dictionary of dictionaries containing interesting groups, represented as a SMILES string;
modifiers_HR: a dictionary with keys==a smiles string, and the value being the human-readable name for the string
"""

"""
Try to generate a reasonable guess for base, transition state, and final. 
Doing all 3 steps at once allows us to do transition state searches without worrying about
the relative numbering of atoms, which is a humongous pain to do manually
"""

with open("list_of_templates.SMILES",'r') as f:
    templates = f.read()

templates = eval(templates)

NUMRGROUPS = 2

"""it is convenient to draw the basic structure using something like molview.org and copy the smiles strings.
use an unexpected atom for the templating positions.
"""


def generateOptions(template,modifiers,NUMRGROUPS):
    strings = []
    strings_HR = []
    templates = ["{{R"+str(i+1)+"}}" for i in range(NUMRGROUPS)]
    mods = []
    for key in modifiers.keys():
        mods.extend(modifiers[key])
    for comb in itertools.product(mods,repeat=NUMRGROUPS):
        print(comb)
        c = template.replace("-STRING-","")
        c_HR = "R1-{{R1}}-R2-{{R2}}"
        for i,entry in enumerate(comb):
            c = c.replace(templates[i],entry)
            c_HR = c_HR.replace(templates[i],modifiers_HR[entry])
        print(c_HR)
        strings.append(c)
        strings_HR.append(c_HR)
    return strings, strings_HR
def getNumElectrons(mol):
    u = 0
    for atom in mol:
        u = u+atom.number
    return u
def make3D(molstring):
    mymol = pybel.readstring("smi",molstring)
    gen3d = ob.OBOp.FindType("gen3D")
    gen3d.Do(mymol.OBMol,"--best")
    structure =  Gratoms()
    smgraph = to_networkx(molstring,explicit_hydrogen=True)
    for atom in mymol.atoms:
        structure.append(ptable[str(atom.atomicnum)])
        structure[-1].position = atom.coords
    for edge in smgraph.edges:
        #print(edge)
        structure.graph.add_edge(edge[0],edge[1],order=smgraph.get_edge_data(edge[0],edge[1])['order'])
    #print(structure.graph.edges)
    return structure

"""tag=1 indicates this H will be bonded to N eventually at the end of the reaction"""
def correctStartingAdsorptionOfMethane(struct,placeholder="U"):
    methane = make3D("CF")
    #translate the methane to where the placeholder ("U") is.
    moveto = np.array([0,0,0])
    zeroout = np.array([0,0,0])
    
    for atom in methane:
        if atom.symbol=="C":
            zeroout = atom.position
            atom.tag=2 #2 is tagged for the carbon that will one day be attached to the gallium
        if atom.symbol=="F":
            atom.tag=1 #1 is tagged for the hydrogen that will one day be attached to the nitrogen
            atom.symbol="H"
            hvector = atom.position - zeroout #for purposes of aligning the methane
            
    for atom in struct:
        if atom.symbol==placeholder:
            moveto = atom.position
        #search the graph for the nitrogen bonded to Ga and Placeholder
        if atom.symbol=="N":
            neighbours = list(struct.graph[atom.index])
            neighsym = [struct[n].symbol for n in neighbours]
            if (placeholder in neighsym) and ("Ga" in neighsym):
                #print(neighsym)
                atom.tag=1 #tags 1 and 1 will eventually kiss
        if atom.symbol=="Ga":
            atom.tag=2 #for the carbon
    #obtain the geometric information for placing the methane
    for atom in struct:
        if atom.symbol=="Ga":
            neighbours = list(struct.graph[atom.index])
            for n in neighbours:
                if struct[n].tag==1: #our nitrogen!
                    targn = struct[n]
                    targvector = targn.position - atom.position
                    
    
    methane.translate(-zeroout)
    #rotate the methane such that the reactive H is pointed in the direction of the nitrogen of interest
    #this is much easier now that the methane is centered at (0,0,0)
    #print(hvector,targvector)
    methane.rotate(hvector,targvector,center=(0,0,0))
    methane.translate(moveto)
    retstruct = struct.copy()
    reconstructedEdges = []
    reconstructedEdgesData = []
    for item in list(methane.graph.edges):
        #print(item)
        edge = list(item)
        tempedge = []
        for node in edge:
            node = node+len(struct)
            tempedge.append(node)
        reconstructedEdges.append(tuple(tempedge))
        order = methane.graph.get_edge_data(edge[0],edge[1])
        reconstructedEdgesData.append(order)
        #this assumes that atom-by-atom append conserves atom order; should be valid
        #given the behaviour of the graph in atoms...
    for atom in methane: #adding two Gratoms objects together DOES NOT preserve atom order in the graph rep!!
        #hence the hullaballoo about reconstructedEdges
        retstruct.append(atom)
    for edge,data in zip(reconstructedEdges,reconstructedEdgesData):
        #print(edge,data)
        retstruct.graph.add_edge(edge[0],edge[1],order=data['order'])
    for atom in retstruct:
        if atom.symbol==placeholder:
            pdx = atom.index
            neighbours = list(retstruct.graph[atom.index])
            #print(neighbours,retstruct.graph.edges)
            #nx.draw(retstruct.graph,with_labels=True)
            for n in neighbours:
                #print(n,pdx)
                retstruct.graph.remove_edge(n,pdx)
    retstruct.pop(pdx)
    return retstruct
def correctEnndingAdsorptionOfMethane(struct,htag=1,gatag=2):
    """takes a structure prepared, tagged and beautified by correctStartingAdsorptionOfMethane
    and creates the end structure using molecules.get_3D_position() after making the correct
    addition of edges in gratoms
    """
    retstruct = struct.copy()
    hnbond = []
    cgabond = []
    breakbond = []
    for atom in retstruct:
        if atom.tag==htag:
            hnbond.append(atom.index)
            if atom.symbol=="H":
                breakbond.append(atom.index)
        if atom.tag==gatag:
            cgabond.append(atom.index)
            if atom.symbol=="C":
                breakbond.append(atom.index)
    retstruct.graph.add_edges_from([hnbond,cgabond],order=1)
    retstruct.graph.remove_edges_from([breakbond])
    #get_3D_positions really sucks compared to the obmol's capabilities...
    #molecules.get_3D_positions(retstruct)
    return retstruct
def makeTS(struct):
    """this is super problem dependent. Basically, I'm going to use the make3D() to make a 3D structure of what 
    I want, with Si and F as the standins for C and H. Then, I will recenter and realign my molecules along 
    a common frame of reference. Finally, I will move my atoms in  my real object to be in the position
    guessed by obmol.
    By keeping a placeholder, I can identify which hydrogens need to move. 
    """

def main(template):
    
    
    startHRs = []
    startStructs = []
    endStructs = []
    
    basalMoles,basalMoles_HR = generateOptions(template,modifiers,NUMRGROUPS)
    for i,mole in enumerate(basalMoles):
        struct = make3D(mole)
        print(mole)
        startStruct = correctStartingAdsorptionOfMethane(struct)
        endStruct = correctEnndingAdsorptionOfMethane(startStruct)
        endStruct = gratoms_relax.relax(endStruct)
        startHRs.append(basalMoles_HR)
        startStructs.append(startStruct)
        endStructs.append(endStruct)
        struct.calc = None #probably psi4 or something
            
#main()