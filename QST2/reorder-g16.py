from ase import io
import numpy as np
from ase.visualize import view
from ase import Atoms, Atom
import itertools
#takes end and start and reorders the atoms such that the atom numbering is correct for use in a 
#QST2 (or a handmade QST3) calculation

#this algorithm is impossible for larger systems or where the atoms dramatically change shape


satoms = io.read("start.gjf")
satoms.pbc = (True,True,True)
#satoms.cell = np.eye(3)*np.array([10,10,10,])
#satoms.center()

eatoms = io.read("end.gjf")
eatoms.pbc = (True,True,True)
#eatoms.cell = np.eye(3)*np.array([10,10,10])
#eatoms.center()

#check that the order is correct
#This is an O(N^2) operation!! I don't know what would be a quicker way to check all the slab atoms are aligned correctly though :(
def center(eatoms,satoms):
    """You should implement center() according to your system's need.I know my systems have 1 Ga, nd
    Nitrogen is always in it. The 3 atoms are correctly ordered in both start and end, and form 
    a fixed reference point for a translation and rotation to as closely as possible match start and
    end geometries.
    
    
    """
    assert eatoms[0].symbol==satoms[0].symbol=='Ga', "atom 0 of end, or of start, is not gallium, or they don't match!"
    assert eatoms[1].symbol==satoms[1].symbol=='N',"atom 1 of end, or of start, is not n, or they don't match!"
    assert eatoms[2].symbol==satoms[2].symbol=='N',"atom 2 of end, or of start, is not n, or they don't match!"

    edist = eatoms[0].position
    sdist = satoms.copy()[0].position
    
#aligning the atoms
    for eatom in eatoms:
        eatom.position = eatom.position-edist
    
    for satom in satoms: 
        satom.position = satom.position-sdist
    satoms.rotate(satoms[1].position,(0,0,1),center=(0,0,0))
    eatoms.rotate(eatoms[1].position,(0,0,1),center=(0,0,0))
    spoint1 = satoms[2].position/dist(satoms[2],satoms[0])
    epoint1 = eatoms[2].position/dist(eatoms[2],eatoms[0])
    angle = np.arccos(np.dot(epoint1,spoint1))
    angle = 180*angle/np.pi
    satoms.rotate(-angle,(0,0,1,),center=(0,0,0))
center(eatoms,satoms)



slabsym = ["Ga","N","C","H","O","I"]
#move all the nonslab atoms to the back of the list for satoms, eatoms
"""for atoms in [satoms,eatoms]:
    for atom in atoms:
        if atom.symbol not in slabsym:
            move = atoms.pop(atom.index)
            atoms.append(move)
            """
reorder = {"satoms":[],"eatoms":[]} 
#dict with satoms being the list of index of the closest atom to where the corresponding entry in eatoms is
#corrected for symbol

def findLikeliestAtom(idx,satoms,eatoms):
    myatom = eatoms[idx]
    closest = 1e3
    retsidx = -1
    for satom in satoms:
        if satom.symbol==myatom.symbol:
            dist = myatom.position - satom.position
            ddist = np.sqrt(dist.dot(dist))
            #print(ddist)
            if ddist<closest:
                closest = ddist
                retsidx = satom.index
    return retsidx
def dist(a,b):
    v = a.position - b.position
    return np.sqrt(v.dot(v))

def mapAtoms(satoms,eatoms):
    """create a mapping from indexes of satoms to indexes of eatoms
    that minimizes the overall geometric distance between atoms of satoms and eatoms.
    
    for every atom in satoms, up to 6 possible matchings to eatoms is created. This is then considered 
    combinatorially to get the overall lowest score. 
    """
    mappings = {}
    """
    grab all atoms from eatom and get their distances, then sort by lowest distance and put into mappings
    """
    for atom in satoms:
        possibleMappings = {} #{eatom_idx:[satom index]}
        #possible mappings for a satom to eatoms.
        for eatom in eatoms:
            if atom.symbol == eatom.symbol:
                possibleMappings.update({eatom.index:dist(atom,eatom)})
        indexlist = []
        distlist = []
        #print(possibleMappings)
        for k in possibleMappings.keys():
            #unravelling and sorting the possible mapping
            if len(indexlist)==0:
                indexlist.insert(0,k)
                distlist.insert(0,possibleMappings[k])

            for ptr,distance in enumerate(distlist):
                #print(min(distlist),indexlist)
                if possibleMappings[k]>=distance:
                    pass
                else:
                    #print("'",k,"' loop inserted at",ptr,"for ",atom.index)
                    distlist.insert(ptr,possibleMappings[k])
                    indexlist.insert(ptr,k)
                    break
                if possibleMappings[k]<min(distlist):
                    print("mapping")
                    distlist.insert(distlist.index((min(distlist))) ,possibleMappings[k])
                    indexlist.insert(distlist.index((min(distlist))),k)

                if ptr==(len(distlist)-1) and (k not in indexlist):
                    #print("'",k,"'inserted at",ptr+1,"for ",atom.index)
                    distlist.insert(ptr+1,possibleMappings[k])
                    indexlist.insert(ptr+1,k)
                    break
        mappings.update({atom.index:indexlist})
    return mappings

def getBestMapping(mappings):
    string = []
    """sadly, evaluating all the best possible combinations would lead to *billions* of evaluations, most of
    which will suck. We shall simply go for the greediest search - the leftmost column is probably the best, 
    so use it, unless an impossible map presents itself (ie nonbijection), in which case, use the left+1, element.
    """
    for k in mappings.keys():
        for element in mappings[k]:
            if element not in string:
                string.append(element)
                break
    return string

maps = mapAtoms(satoms,eatoms)
desiredImage = getBestMapping(maps)

def reorder(image,eatoms):
    """reorder eatoms by desired image
    """
    eatomsC = eatoms.copy()
    mlist = [ea for ea in eatoms]
    for idx,key in enumerate(image):
        mlist[idx].index = key
        #print(mlist[idx],key)
    
    return Atoms(mlist)
neatoms = reorder(desiredImage,eatoms)
io.write("end-fix.gjf",neatoms)
io.write("start-fix.gjf",satoms)

print("DO NOT BLINDLY TRUST THE RESULTS OF THIS PROGRAM! Ambiguous cases, or cases where elements are mirrored accross a plane will result in INCORRECT assignments. Use gaussview's z-matrix or coord list tool to examine!")
#soccupied is the image, eocc is the image
#convert newE[eocc] = newE[socc], with the appropriate changing of the index tag. Assert that there are no 
#unrepresented objects (which should be taken care of by removing duplicates from soccupied)
"""def rearrange(socc,eocc):
    reorder = {"satoms":[],"eatoms":[]} 
    ceatoms = eatoms.copy()
    newE = [i for i in eatoms]
    for entry in reorder['satoms']:
        newE[entry]="PLACEHOLDER"
    for idx,atidx in enumerate(eocc): #socc is preimage of checkorder, eocc is image
        soccP = socc[idx]
        newE[soccP] = ceatoms[atidx] #invert the function
    r,socc,eocc,ambiguities = checkAndReOrder(reorder,Atoms(newE),satoms)
    return Atoms(newE),r,ambiguities
    
def fixed(r,a):
    if len(r['satoms'])>0:
        isCuzAmbigous = True
        for entry in r['satoms']:
            if entry in a:
                pass
            else:
                isCuzAmbigous = False
                print("failed to arrange correctly:",r,a)
                return False
        else:
            print("Ambiguous situation left:",r,a)
            return True
    else:
        return True

for i in range(3):
    eeatoms,r,a = rearrange(socc,eocc)
    print(i)
    if fixed(r,a):
        io.write("end-fix.xyz",eeatoms)
        io.write("start-fix.xyz",satoms)
        view(eeatoms)
        view(satoms)
        break
"""