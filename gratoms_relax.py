#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 06:28:47 2022

Gratoms comes with get_3D_positions to create a 3D structure from a graph, but it *really* sucks

This uses obmol's much more sophisticated forcefield (ghemical) to relax a ase structure from its graph,
and crucially, preserves atom order. 
"""


import ase
from ase.io import read, write
from ase.calculators.gaussian import Gaussian,GaussianOptimizer
from catkit import Gratoms
from catkit.gen import molecules
from openbabel import pybel
import openbabel as ob
import itertools
import os
from pysmiles import read_smiles as to_networkx
import networkx as nx
import numpy as np
ptable = {'0':'X','1': 'H', '2': 'He', '3': 'Li', '4': 'Be', '5': 'B', '6': 'C', '7': 'N', '8': 'O', '9': 'F', '10': 'Ne', '11': 'Na', '12': 'Mg', '13': 'Al', '14': 'Si', '15': 'P', '16': 'S', '17': 'Cl', '18': 'Ar', '19': 'K', '20': 'Ca', '21': 'Sc', '22': 'Ti', '23': 'V', '24': 'Cr', '25': 'Mn', '26': 'Fe', '27': 'Co', '28': 'Ni', '29': 'Cu', '30': 'Zn', '31': 'Ga', '32': 'Ge', '33': 'As', '34': 'Se', '35': 'Br', '36': 'Kr', '37': 'Rb', '38': 'Sr', '39': 'Y', '40': 'Zr', '41': 'Nb', '42': 'Mo', '43': 'Tc', '44': 'Ru', '45': 'Rh', '46': 'Pd', '47': 'Ag', '48': 'Cd', '49': 'In', '50': 'Sn', '51': 'Sb', '52': 'Te', '53': 'I', '54': 'Xe', '55': 'Cs', '56': 'Ba', '57': 'La', '58': 'Ce', '59': 'Pr', '60': 'Nd', '61': 'Pm', '62': 'Sm', '63': 'Eu', '64': 'Gd', '65': 'Tb', '66': 'Dy', '67': 'Ho', '68': 'Er', '69': 'Tm', '70': 'Yb', '71': 'Lu', '72': 'Hf', '73': 'Ta', '74': 'W', '75': 'Re', '76': 'Os', '77': 'Ir', '78': 'Pt', '79': 'Au', '80': 'Hg', '81': 'Tl', '82': 'Pb', '83': 'Bi', '84': 'Po', '85': 'At', '86': 'Rn', '87': 'Fr', '88': 'Ra', '89': 'Ac', '90': 'Th', '91': 'Pa', '92': 'U', '93': 'Np', '94': 'Pu', '95': 'Am', '96': 'Cm', '97': 'Bk', '98': 'Cf', '99': 'Es', '100': 'Fm', '101': 'Md', '102': 'No', '103': 'Lr', '104': 'Rf', '105': 'Db', '106': 'Sg', '107': 'Bh', '108': 'Hs', '109': 'Mt', '110': 'Ds', '111': 'Rg', '112': 'Cn', '113': 'Nh', '114': 'Fl', '115': 'Mc', '116': 'Lv', '117': 'Ts', '118': 'Og'}


def makeGratom3D(molstring):
    mymol = pybel.readstring("smi",molstring)
    gen3d = ob.OBOp.FindType("gen3D")
    gen3d.Do(mymol.OBMol,"--best")
    structure =  Gratoms()
    smgraph = to_networkx(molstring,explicit_hydrogen=True)
    for atom in mymol.atoms:
        structure.append(ptable[str(atom.atomicnum)])
        structure[-1].position = atom.coords
    for edge in smgraph.edges:
        #print(edge)
        structure.graph.add_edge(edge[0],edge[1],order=smgraph.get_edge_data(edge[0],edge[1])['order'])
    #print(structure.graph.edges)
    return structure

def appendGratom(struct1,struct2):
    retstruct = struct1.copy()
    reconstructedEdges = []
    reconstructedEdgesData = []
    for item in list(struct2.graph.edges):
        #print(item)
        edge = list(item)
        tempedge = []
        for node in edge:
            node = node+len(retstruct)
            tempedge.append(node)
        reconstructedEdges.append(tuple(tempedge))
        order = struct2.graph.get_edge_data(edge[0],edge[1])
        reconstructedEdgesData.append(order)
    for atom in struct2: #adding two Gratoms objects together DOES NOT preserve atom order in the graph rep!!
        #hence the hullaballoo about reconstructedEdges
        retstruct.append(atom)
    for edge,data in zip(reconstructedEdges,reconstructedEdgesData):
        #print(edge,data)
        retstruct.graph.add_edge(edge[0],edge[1],order=data['order'])
    return retstruct


def relax(GratomStruct):
    #this will not only relax the structure, it will also preserve the atom order! Crucial, as this is
    #not the case when generating it from a SMILES string!
    mymol = ob.OBMol()
    for atom in GratomStruct:
        a = mymol.NewAtom()
        a.SetAtomicNum(int(atom.number))
        a.SetVector(atom.position[0],atom.position[1],atom.position[2])
    for edge in GratomStruct.graph.edges:
        order = GratomStruct.graph.get_edge_data(edge[0],edge[1])['order'] 
        mymol.AddBond(int(edge[0]+1),int(edge[1]+1),int(order)) #pybel indexes from 1 for some reason...
    gen3d = ob.OBOp.FindType("gen3D")
    gen3d.Do(mymol,"--fast")
    structure =  Gratoms()
    for atom in ob.pybel.Molecule(mymol):
        structure.append(ptable[str(atom.atomicnum)])
        structure[-1].position = atom.coords
    for edge in GratomStruct.graph.edges:
        structure.graph.add_edge(edge[0],edge[1],order=GratomStruct.graph.get_edge_data(edge[0],edge[1])['order'])
    return structure
    """mol = openbabel.OBMol()
    a = mol.NewAtom()
    a.SetAtomicNum(6)   # carbon atom
    a.SetVector(0.0, 1.0, 2.0) # coordinates
    b = mol.NewAtom()
    mol.AddBond(1, 2, 1)   # atoms indexed from 1"""